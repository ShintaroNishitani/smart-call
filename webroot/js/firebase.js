var firebaseConfig = {
	apiKey: "AIzaSyDzRb6A9H-GQBfmYBXu_Ig0kP2Wry8fMoA",
	authDomain: "smart-call-282105.firebaseapp.com",
	databaseURL: "https://smart-call-282105.firebaseio.com",
	projectId: "smart-call-282105",
	storageBucket: "smart-call-282105.appspot.com",
	messagingSenderId: "812804237714",
	appId: "1:812804237714:web:e6e99dadd315bdf8fcb94b"
};
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
messaging.usePublicVapidKey('BEBRsLNIYImLCePFgoxCxCUE6OmqxNDHNmam22tkind-4sFxs2CWMUk2feRBtQ2fqxVb9FYgocyqrNx0l_JI41E');// VAPIDを設定

var db = firebase.firestore();
var usersRef = db.collection("users");

// フォアグラウンドでのプッシュ通知受信
messaging.onMessage(function(payload) {
	console.log("onMessage");

	// var notificationTitle = payload.data.title; // タイトル
	// var notificationOptions = {
	// 	body: payload.data.body,
	// 	icon: payload.data.icon,
	// 	click_action: payload.data.url
	// };

	// if (!("Notification" in window)) {
	// 	// ブラウザが通知機能に対応しているかを判定
	// } else if (Notification.permission === "granted") {
	// 	// 通知許可されていたら通知する
	// 	var notification = new Notification(notificationTitle,notificationOptions);
	// }
	location.reload(true);
});


checkSubscription();

// 通知チェック
function checkSubscription() {

	//通知の承認を確認
	messaging.requestPermission().then(function() {
		//トークンを確認
		messaging.getToken().then(function(token) {
			//トークン発行
			if (token) {
				//トークンがDBに入っているか確認
				usersRef.where('token', '==', token).get().then(function(oldLog){
					if(oldLog.empty){
						console.log('トークンは登録されていません。');
					} else {
						//入っていれば購読状況確認
						console.log('トークンはすでに登録されています。');
						oldLog.forEach(function(doc){
							var data = doc.data();
							if(data.subscribe == true){
								// 通知ON
								//　↓でならないので調べる
								// $("#notify").prop("checked", true);
								notifyOn();
							}
						});
					}
				});
			} else {
					console.log('通知の承認が得られませんでした。');
			}
		}).catch(function(err) {
				console.log('トークンを取得できませんでした。', err);
		});

	}).catch(function (err) {
		//プッシュ通知未対応
			console.log('通知の承認が得られませんでした。', err);
	});

}

// 通知ON
function getSubscription() {
	//通知の承認を確認
	messaging.requestPermission().then(function() {
			//トークンを確認
			messaging.getToken().then(function(token) {
				if (token) {
					//トークンがDBに入っているか確認
					usersRef.where('token', '==', token).get().then(function(oldLog){
						if(oldLog.empty){
							//トークン登録がなければトークン登録
							usersRef.add({
								token: token,
								shop_id: shop_id,
								subscribe: true
							});
							console.log('トークン新規登録しました。');
						} else {
							//トークン登録があれば通知ONに変更
							oldLog.forEach(function(doc){
								console.log('トークンはすでに登録されています。');
								usersRef.doc(doc.id).update({
									subscribe: true
								})
							});
						}
						notifyOn();
					});

				} else {
					console.log('通知の承認が得られませんでした。');
				}
			}).catch(function(err) {
				console.log('トークンを取得できませんでした。', err);
			});
	}).catch(function (err) {
		console.log('通知の承認が得られませんでした。', err);
	});
}

// 通知OFF
function removeSubscription() {
	//通知の承認を確認
	messaging.requestPermission().then(function() {
		//トークンを確認
		messaging.getToken().then(function(token) {
			//トークン発行
			if (token) {
				//トークンがDBに入っているか確認
				usersRef.where('token', '==', token).get().then(function(oldLog){
					if(oldLog.empty){
						console.log('トークンは登録されていません。');
					} else {
						//トークン登録があれば通知の解除を行う
						oldLog.forEach(function(doc){
							usersRef.doc(doc.id).update({
								subscribe: false
							})
							.then(function() {
								console.log("通知を解除しました。");
								notifyOff();
							}).catch(function(error) {
								console.error("Error removing document: ", error);
							});
						});
					}
				});
			} else {
				console.log('トークンを取得できませんでした。');
			}
		}).catch(function(err) {
			console.log('トークンを取得できませんでした。', err);
		});
	}).catch(function (err) {
		console.log('通知の承認が得られませんでした。', err);
	});
}

function notifyOn () {
	$("#notify").text("通知中");
	$("#notify").removeClass("btn-default");
	$("#notify").addClass("btn-danger");
}

function notifyOff () {
	$("#notify").text("通知OFF");
	$("#notify").removeClass("btn-danger");
	$("#notify").addClass("btn-default");
}
