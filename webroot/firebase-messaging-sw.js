importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-messaging.js');

var firebaseConfig = {
	apiKey: "AIzaSyDzRb6A9H-GQBfmYBXu_Ig0kP2Wry8fMoA",
	authDomain: "smart-call-282105.firebaseapp.com",
	databaseURL: "https://smart-call-282105.firebaseio.com",
	projectId: "smart-call-282105",
	storageBucket: "smart-call-282105.appspot.com",
	messagingSenderId: "812804237714",
	appId: "1:812804237714:web:e6e99dadd315bdf8fcb94b"
};
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();


// バックグラウンドでのプッシュ通知受信
// ※送信されてきた情報に通知フィールド(notification)が存在する場合はsetBackgroundMessageHandlerが呼び出されないので注意
messaging.setBackgroundMessageHandler(function(payload) {
	console.log('setBackgroundMessageHandler');
	// var title = payload.data.title;
	// var options = {
	// 	body: payload.data.body,
	// 	icon: payload.data.icon,
	// 	click_action: payload.data.url
	// };

	// return self.registration.showNotification(title, options);
	location.reload(true);

});

let baseURL = '/';

// PUSH通知受信処理
self.addEventListener("push", function(event) {
	console.log('addEventListener');

	var json = event.data.json();

	baseURL = json.data.url;

	var title = json.data.title;
	var options = {
		body: json.data.body,
		icon: json.data.icon,
	};

	event.waitUntil(self.registration.showNotification(title, options));

});

// notificationクリックイベント
self.addEventListener('notificationclick', event => {
	self.clients.openWindow('/');

	event.notification.close();
});
