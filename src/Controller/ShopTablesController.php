<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * ShopTables Controller
 *
 * @property \App\Model\Table\ShopTablesTable $ShopTables
 *
 * @method \App\Model\Entity\ShopTable[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShopTablesController extends AppController
{
    /**
     * テーブル管理
     */
    public function index()
    {
        $this->set('title', __('テーブル管理'));

        $query = $this->ShopTables->find()->where(['ShopTables.shop_id'=>$this->Auth->user()['shop_id']]);
        $shopTables = $this->paginate($query);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!$this->request->getData('id')) {
                $data = $this->ShopTables->newEntity();
            } else {
                $data = $this->ShopTables->get($this->request->getData('id'));
            }
            $data = $this->ShopTables->patchEntity($data, $this->request->getData());
            $data->shop_id = $this->Auth->user()['shop_id'];
            if ($this->ShopTables->save($data)) {
                $this->Flash->success(__('テーブルが登録されました。'));
                return $this->redirect(['action' => 'index']);

            } else {
                $this->log($data->errors());
                $this->Flash->error(__('テーブルの登録に失敗しました。'));
            }

        }

        $this->set(compact('shopTables'));
    }

    /**
     * テーブル登録
     */
    public function edit($id = null)
    {
        $this->autoRender = false;

        $data = $this->ShopTables->newEntity();
        if ($id) {
            $data = $this->ShopTables->get($id);
        }
        echo json_encode(compact('data'));
    }

    /**
     * テーブル削除
     */
    public function delete($id = null)
    {
        $data = $this->ShopTables->get($id);
        $data->enable = 0;
        if ($this->ShopTables->save($data)) {
            $this->Flash->success(__('テーブルを削除しました。'));
        } else {
            $this->Flash->error(__('テーブルの削除に失敗しました。'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * QRコード印刷
     */
    public function qr($id = null)
    {
        $this->viewBuilder()->setLayout('none');

        $url = Router::url('/', true) . "calls/request/". $this->Auth->user()['shop_id'] . "/". $id;

        $this->set(compact('url'));

    }
}
