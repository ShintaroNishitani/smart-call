<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contents Controller
 *
 * @property \App\Model\Table\ContentsTable $Contents
 *
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentsController extends AppController
{
    /**
     * 呼び出し種別管理
     */
    public function index()
    {
        $this->set('title', __('呼び出し種別管理'));

        $query = $this->Contents->find()->where(['Contents.shop_id'=>$this->Auth->user()['shop_id']]);
        $contents = $this->paginate($query);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!$this->request->getData('id')) {
                $data = $this->Contents->newEntity();
            } else {
                $data = $this->Contents->get($this->request->getData('id'));
            }
            $data = $this->Contents->patchEntity($data, $this->request->getData());
            $data->shop_id = $this->Auth->user()['shop_id'];
            if ($this->Contents->save($data)) {
                $this->Flash->success(__('呼び出し種別が登録されました。'));
                return $this->redirect(['action' => 'index']);

            } else {
                $this->log($data->errors());
                $this->Flash->error(__('呼び出し種別の登録に失敗しました。'));
            }

        }

        $this->set(compact('contents'));
    }

    /**
     * 呼び出し種別登録
     */
    public function edit($id = null)
    {
        $this->autoRender = false;

        $data = $this->Contents->newEntity();
        if ($id) {
            $data = $this->Contents->get($id);
        }
        echo json_encode(compact('data'));
    }

    /**
     * 呼び出し種別削除
     */
    public function delete($id = null)
    {
        $data = $this->Contents->get($id);
        $data->enable = 0;
        if ($this->Contents->save($data)) {
            $this->Flash->success(__('呼び出し種別を削除しました。'));
        } else {
            $this->Flash->error(__('呼び出し種別の削除に失敗しました。'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
