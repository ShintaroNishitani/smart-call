<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;
use Google\Cloud\Firestore\FirestoreClient;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Accounts',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Calls',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Accounts',
                'action' => 'login'
            ],
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Accounts',
                    'fields' => ['username' => 'mail', 'password' => 'password']
                ]
            ],
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
      $this->Auth->allow(['add', 'request', 'complete']);
    }

    /**
     * プッシュ通知
     */
    public function pushNotify($shop_id, $title, $body) {
        $db = new FirestoreClient(['projectId' => 'smart-call-282105']);
        $usersRef = $db->collection('users');
        $users = $usersRef->documents();
        $ids = [];
        foreach ($users as $user) {
            // 自店舗のみ
            if ($user['shop_id'] == $shop_id && $user['subscribe'] == true) {
                $ids[] = $user['token'];
            }
        }

        $data = [
                "registration_ids" => $ids,
                "data" => [
                        "title" => $title,
                        "body" => $body,
                        "icon" => "/bell_512.png",
                        "url" => Router::url('/', true) . "calls"
                ],
                "priority" => "high"
        ];

        $header = [
                'Authorization: key=' . env('FCM_API_SEVER_KEY'),
                'Content-Type: application/json',
        ];
        $context = stream_context_create(array(
                'http' => array(
                        'method' => 'POST',
                        'header' => implode(PHP_EOL,$header),
                        'content'=>  json_encode($data),
                        'ignore_errors' => true
                )
        ));

        $response = file_get_contents(env('FCM_API_URL'), false, $context);

        $result = json_decode($response,true);

    }
}
