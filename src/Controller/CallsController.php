<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;

/**
 * Calls Controller
 *
 * @property \App\Model\Table\CallsTable $Calls
 *
 * @method \App\Model\Entity\Call[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CallsController extends AppController
{

    /**
     * 客側の呼び出し画面
     */
    public function request($shop_id, $table_id) {
        $this->viewBuilder()->setLayout('none');

        $call = $this->Calls->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $call = $this->Calls->patchEntity($call, $this->request->getData());
            if ($this->Calls->save($call)) {

                // プッシュ通知
                $call = $this->Calls->find()->contain(['Shops', 'ShopTables', 'Contents'])->where(['Calls.id'=>$call->id])->first();
                $this->pushNotify($shop_id, $call->shop_table->name, $call->content->name);

                return $this->redirect(['action' => 'complete', $call->shop_id, $call->table_id]);
            } else {
                $this->log($data->errors());
                $this->Flash->error(__('店員の呼び出しに失敗しました。'));
            }
        } else {
            if (!$shop_id || !$table_id) {
                throw new NotFoundException(__('User not found'));
            }

            $call->shop_id = $shop_id;
            $call->table_id = $table_id;

            $tblShops = TableRegistry::get('Shops');
            $shop = $tblShops->get($shop_id);
            $tblShopTables = TableRegistry::get('ShopTables');
            $table = $tblShopTables->get($table_id);
            $tblContents = TableRegistry::get('Contents');
            $contents = $tblContents->find()->where(['shop_id'=>$shop_id])->all();

            if (!$shop || !$table) {
                throw new NotFoundException(__('User not found'));
            }
        }
        $this->set(compact('call', 'shop', 'table', 'contents'));

    }

    /**
     * 客側の呼び出し完了画面
     */
    public function complete($shop_id, $table_id) {
        $this->viewBuilder()->setLayout('none');

        $this->set(compact('shop_id', 'table_id'));

    }

    /**
     * 店舗側の呼び出し一覧画面
     */
    public function index()
    {
        $this->set('title', __('CALLING LIST'));

        $this->paginate = [
            'contain' => ['Shops', 'ShopTables', 'Contents'],
        ];
        $calls = $this->Calls->find()->contain(['Shops', 'ShopTables', 'Contents'])->where(['Calls.shop_id'=>$this->Auth->user()['shop_id'], 'Calls.status'=>0])->all();

        $shop_id = $this->Auth->user()['shop_id'];

        $this->set(compact('calls', 'shop_id'));
    }


    /**
     * 店舗側の対応済み処理
     */
    public function checked($id = null)
    {
        $call = $this->Calls->get($id);
        $call->status = 1;
        if (!$this->Calls->save($call)) {
            $this->Flash->error(__('対応済みにできませんでした'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
