<div class="wrapper wrapper-full-page ">
  <div class="full-page section-image" filter-color="black" data-image="/assets/img/bg/fabio-mangione.jpg">
    <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
    <div class="content">
      <div class="container">
        <div class="col-lg-6 col-md-8 ml-auto mr-auto">
          <?= $this->Form->create() ?>
            <div class="card card-login">
              <div class="card-header ">
                <div class="card-header text-center">
                  <h4 class="header text-center">SMART CALL</h4>
                </div>
              </div>
              <div class="card-body ">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="nc-icon nc-single-02"></i>
                    </span>
                  </div>
                  <input id="mail" name="mail" type="text" class="form-control" placeholder="ログインID">
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="nc-icon nc-key-25"></i>
                    </span>
                  </div>
                  <input id="password" name="password" type="password" placeholder="パスワード" class="form-control">
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class='form-check-input' type='checkbox' name='auto_login_check' value=1>
                        <span class="form-check-sign"></span><?= __('自動ログイン'); ?>
                    </label>
                </div>
                <br />
              </div>
              <div class="card-footer text-center">
                <?= $this->Form->button(' '.__('ログイン'), ["type"=>"submit", "class"=>"btn btn-warning btn-round btn-block mb-3"]) ?></br>
              </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
      </div>
    </div>
  </div>
</div>

