<script>
    function deleteData(id) {
        Swal.fire({
            title: '',
            text: "呼び出し種別を削除しますか?",
            type: 'warning',
            showCancelButton: true,
            customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false,
            confirmButtonText: '削除',
            cancelButtonText: 'キャンセル'
        }).then((result) => {
            if (result.value) {
                location.href = "/contents/delete/" + id;
            }
        })
    }

    function editData(id) {
        $.ajax({
            type: "get",
            dataType: 'json',
            url: "/contents/edit/" + id,
            success : function(data){
                if (data && data.data){
                    $('#id').val(data.data.id);
                    $('#name').val(data.data.name);

                    $('#addModal').modal()
                }
            }
        });
    }

</script>
<div class="row">
    <div class="col-md-6">
        <div class="card card-user">
            <div class="card-body">
                <button class="btn btn-outline-success btn-round" onclick="editData(0)"><span class="btn-label"><i class="fa fa-plus"></i></span>
                新規追加
                </button>
                <table class="table table-bordered table-striped text-nowrap">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('呼び出し種別名') ?></th>
                            <th scope="col" class="actions"><?= __('') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($contents as $content): ?>
                        <tr>
                            <td><?= h($content->name) ?></td>
                            <td class="text-center">
                                <button class="btn btn-sm btn-icon btn-round btn-success" onclick="editData(<?= h($content->id) ?>)">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button class="btn btn-sm btn-icon btn-round btn-reddit" onclick="deleteData(<?= h($content->id) ?>)">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('前')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('次') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">呼び出し種別登録</h5>
      </div>
      <?= $this->Form->create() ?>
          <?= $this->Form->hidden('id', ["id"=>"id"]);?>

        <div class="modal-body">
            <div class="form-group">
                <label>呼び出し種別名</label>
                <?= $this->Form->control('name', ["class"=>"form-control", "label"=>false]);?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">閉じる</button>
          <?= $this->Form->button(__('登録'), ["type"=>"submit", "class"=>"btn btn-success"]) ?>
        </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
