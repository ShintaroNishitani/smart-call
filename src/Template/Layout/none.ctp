<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Smart Call
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/assets/css/paper-dashboard.css?v=2.1.1" rel="stylesheet" />
    <?= $this->Html->css('style.css') ?>

    <script src="/assets/js/core/jquery.min.js"></script>
    <script src="/assets/js/core/popper.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>
    <script src="/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <script src="/assets/js/plugins/moment.min.js"></script>
    <script src="/assets/js/plugins/bootstrap-switch.js"></script>
    <script src="/assets/js/plugins/sweetalert2.min.js"></script>
    <script src="/assets/js/plugins/jquery.validate.min.js"></script>
    <script src="/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
    <script src="/assets/js/plugins/bootstrap-selectpicker.js"></script>
    <script src="/assets/js/plugins/bootstrap-datetimepicker.js"></script>
    <script src="/assets/js/plugins/jquery.dataTables.min.js"></script>
    <script src="/assets/js/plugins/bootstrap-tagsinput.js"></script>
    <script src="/assets/js/plugins/jasny-bootstrap.min.js"></script>
    <script src="/assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>
    <script src="/assets/js/plugins/fullcalendar/daygrid.min.js"></script>
    <script src="/assets/js/plugins/fullcalendar/timegrid.min.js"></script>
    <script src="/assets/js/plugins/fullcalendar/interaction.min.js"></script>
    <script src="/assets/js/plugins/jquery-jvectormap.js"></script>
    <script src="/assets/js/plugins/nouislider.min.js"></script>
    <script src="/assets/js/plugins/chartjs.min.js"></script>
    <script src="/assets/js/plugins/bootstrap-notify.js"></script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <?= $this->Flash->render() ?>
    <div class="wrapper ">
      <div class="main-panel" style="width:100%;">
        <div class="content" style="margin-top:20px;">
              <?= $this->fetch('content') ?>
        </div>
      </div>
    </div>
    <footer>
    </footer>
</body>
</html>
