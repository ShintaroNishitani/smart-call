<script type="text/javascript">
    // if (navigator.geolocation) {
    //     navigator.geolocation.getCurrentPosition(
    //     function (pos) {
    //             var locationlat = pos.coords.latitude;
    //             var locationlong = pos.coords.longitude;
    //             $('#location .lat').append(locationlat);
    //             $('#location .long').append(locationlong);
    //             var linktag = 'https://www.google.co.jp/search?q=' + locationlat + ',' + locationlong;
    //             $('#linktag').attr('href',linktag);
    //     });
    // } else {
    //     window.alert("本ブラウザではGeolocationが使えません");
    // }
</script>

<!-- <div id="location">
    <p class="lat">緯度: </p>
    <p class="long">経度: </p>
</div>
<p><a id="linktag" href="" target="_blank">現在地の地図を表示</a></p> -->


<div class="row">
    <div class="col-md-12">
        <?= $this->Form->create($call) ?>
        <?= $this->Form->hidden('shop_id');?>
        <?= $this->Form->hidden('table_id');?>

        <div class="card">
            <div class="card-header text-center">
                <h4 class="card-title">SMART CALL</h4>
            </div>
            <div class="card-body">
                <h5 class=" text-center"><div class="alert alert-default"><span><?= $shop->name;?></br><?= $table->name;?></span></div></h5>
                <div style="margin-bottom:30px;"></div>
                <p class="text-info">ご用件をえらんで<span class="text-danger">店員を呼ぶ</span>ボタンを押してください </p>
                <p class="text-info">Please select your requirements and press the call button </p>
                <div style="margin-bottom:30px;"></div>
                <div class="row">
                    <h5>
                    <div class="col-sm-12 checkbox-radios">
                        <?php
                            $i = 0;
                            foreach ($contents as $content) {
                        ?>
                            <div class="form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="content_id" value=<?= $content->id;?> <?= $i==0?'checked':'';?>>
                                        <?= $content->name;?>
                                    <span class="form-check-sign"></span>
                                </label>
                            </div>
                        <?php
                                $i++;
                            }
                        ?>
                    </div>
                    </h5>
                </div>
            </div>
            <div class="card-footer text-center">
                <?= $this->Form->button(' '.__('店員を呼ぶ Call'), ['confirm' => __('店員を呼びますか？'), "id"=>"submit", "type"=>"submit", "class"=>"btn btn-lg btn-success btn-round fa fa-bell-o"]) ?>
            </div>
            <div style="margin-bottom:30px;"></div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>


