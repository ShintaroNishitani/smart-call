<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="cly7796.net PWA sample">
<link rel="apple-touch-icon" href="/img/bell_512.png">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
    // firebase登録用
    var shop_id='<?php echo $shop_id; ?>';

    // SW登録
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/firebase-messaging-sw.js')
                .then((reg) => {
                    console.log('Service worker registered.', reg);
                });
        });
    }

    $(function(){
        // $('#notify').on('change.bootstrapSwitch', function(){
        //     if ($(this).prop("checked")) {
        //         getSubscription();
        //     } else {
        //         removeSubscription();
        //     }
        // });
        $('#notify').on('click', function(){
            var className = $(this).attr("class");
            if ( className.match(/danger/)) {
                removeSubscription();
            } else {
                getSubscription();
            }
        });
    });
</script>

<script defer src="https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js"></script>
<script defer src="https://www.gstatic.com/firebasejs/7.15.5/firebase-messaging.js"></script>
<script defer src="https://www.gstatic.com/firebasejs/7.15.5/firebase-firestore.js"></script>
<script defer src="/js/firebase.js"></script>

<div class="row">
    <div class="col-md-8">
        <div class="card card-user">
            <div class="card-body">
                <span class="badge badge-warning"><?= h(count($calls)) ?>件</span>
                <div class="table-responsive-sm">
                <p class="text-info text-center">呼び出し順に表示されます <br/>対応後にOKボタンを押してください </p>
                <div class="col-md-4">
                <button id="notify" class="btn btn-sm btn-default">通知OFF</button>
                <!-- <p class="category">通知</p> -->
                <!-- <input id="notify" class="bootstrap-switch" type="checkbox" data-toggle="switch" data-on-color="primary" data-off-color="default" data-on-label="ON" data-off-label="OFF"> -->
                </div>
                <table class="table text-nowrap">
                    <thead>
                        <tr>
                            <th class="text-center"><?= __('順番') ?></th>
                            <th class="text-center"><?= __('テーブル') ?></th>
                            <th><?= __('') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i = 1;
                            foreach ($calls as $call):
                        ?>
                        <tr>
                            <td class="text-center"><?= h($i++) ?></td>
                            <td><?= h($call->shop_table->name) ?><br/><small class="text-danger"><?= h($call->content->name) ?></small></td>
                            <td class="text-center">
                                <?= $this->Form->postLink('<i class="fa fa-check"></i> OK', ['action' => 'checked', $call->id], ['confirm' => __('対応済みにしますか？'), 'escape' => false, "class"=>"btn btn-success btn-round"]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

