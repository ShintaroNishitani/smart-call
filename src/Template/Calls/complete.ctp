<div class="row">
    <div class="col-md-12">
        <div style="margin-bottom:50px;"></div>
        <div class="text-center">
            <img class="" src="/img/complete.png">
        </div>
        <div style="margin-bottom:50px;"></div>
        <div class="text-center">
            <h3>店員を呼びました。<br/>しばらくお待ち下さい。</h3>
            <h5>You called the staff<br/>Please wait a moment</h5>
        </div>
        <div style="margin-bottom:50px;"></div>
        <div class="text-center">
            <?= $this->Html->link('<i class="fa fa-check"></i>', ['action' => 'request', $shop_id, $table_id], ['escape' => false, "class"=>"btn btn-lg btn-icon btn-round btn-success"]) ?>
        </div>
    </div>
</div>
